import java.io.*;
import java.util.*;
public class example {
	public static int[] getArray(int size) {
		int[] array = new int[size];
		Random rand = new Random();
		for(int i = 0; i < size; i++) {
			array[i] = i;
		}
		return array;
	}
	public static int[] getArray2(int size) {
		int[] array = new int[size];
		Random rand = new Random();
		for(int i = 0; i < size; i++) {
			array[i] = size-i;
		}
		return array;
	}
	public static HashMap getMap(int size) {
		HashMap<Integer,Integer> map= new HashMap<Integer,Integer>();
		Random rand = new Random();
		for(int i = 0; i < size; i++) {
			map.put(i,size-i);
		}
		return map;
	}

	public static void findRandom(int num){
		HashMap<Integer,Integer> NumMap = new HashMap<Integer,Integer>();
		int[] numArray;
		int[] numArray2;
		int key;
		int key2;
		
		long startTime;
		long endTime;
		Set set;
		Iterator i;
		Random rand = new Random();
		boolean bool;
		numArray = new int[num];
		numArray2 = new int[num];
		NumMap = getMap(num);
		numArray = getArray(num);
		numArray2 = getArray2(num);
		
		startTime = System.nanoTime();
		key = rand.nextInt(num);
		key2 = NumMap.get(key);
		
		endTime = System.nanoTime();
		System.out.print(num+"         "+(endTime-startTime));
		
		startTime = System.nanoTime();
		key = rand.nextInt(num);
		for(int il = 0;il < num; il++) {
			if(numArray[il] == key) {
				break;
			}
		}
		key = rand.nextInt(num);
		for(int i2 = 0;i2 < num; i2++) {
			if(numArray2[i2] == key) {
				break;
			}
		}
		endTime = System.nanoTime();
		System.out.println(num+"            "+(endTime-startTime)+"\n");
	}
	public static void deleteRandom(int num){
		HashMap<Integer,Integer> NumMap = new HashMap<Integer,Integer>();
		int[] numArray;
		int[] numArray2;
		int key;
		int key2;
		long startTime;
		long endTime;
		Set set;
		Iterator i;
		Random rand = new Random();
		boolean bool;
		numArray = new int[num];
		numArray2 = new int[num];
		NumMap = getMap(num);
		numArray = getArray(num);
		numArray2 = getArray2(num);
		
		startTime = System.nanoTime();
		key = rand.nextInt(num);
		key2 = NumMap.remove(key);
		
		endTime = System.nanoTime();
		System.out.print(num+"         "+(endTime-startTime));

		startTime = System.nanoTime();
		key = rand.nextInt(num);
		for(int il = 0;il < num; il++) {
			if(numArray[il] == key) {
				numArray = null;
				break;
			}
		}
		key = rand.nextInt(num);
		for(int i2 = 0;i2 < num; i2++) {
			if(numArray2[i2] == key) {
				numArray2 = null;
				break;
			}
		}
		endTime = System.nanoTime();
		System.out.println(num+"            "+(endTime-startTime)+"\n");
	}
	public static void main(String[] args) {
		
		System.out.println("Time taken to find a random number in the two structures.");
		System.out.println("               HashMap              Arrays");
		findRandom(100);
		findRandom(1000);
		findRandom(10000);
		findRandom(100000);
		
		//Operation will noe be to delete a random number from the structures
		System.out.println("Time taken to delete a random number in the two structures.");
		System.out.println("               HashMap              Arrays");
		deleteRandom(100);
		deleteRandom(1000);
		deleteRandom(10000);
		deleteRandom(100000);
	}
}